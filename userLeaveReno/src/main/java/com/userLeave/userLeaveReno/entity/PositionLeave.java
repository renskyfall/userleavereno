package com.userLeave.userLeaveReno.entity;
// Generated Apr 7, 2020 3:19:01 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * PositionLeave generated by hbm2java
 */
@Entity
@Table(name = "position_leave", schema = "public")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createddate5", "updateddate5"}, allowGetters = true)
public class PositionLeave implements java.io.Serializable {

	private long idpositionleave;
	private Positions positions;
	private Short jumlahcuti;
	private String createdby5;
	private Date createddate5;
	private String updatedby5;
	private Date updateddate5;

	public PositionLeave() {
	}

	public PositionLeave(long idpositionleave) {
		this.idpositionleave = idpositionleave;
	}

	public PositionLeave(long idpositionleave, Positions positions, Short jumlahcuti, String createdby5,
			Date createddate5, String updatedby5, Date updateddate5) {
		this.idpositionleave = idpositionleave;
		this.positions = positions;
		this.jumlahcuti = jumlahcuti;
		this.createdby5 = createdby5;
		this.createddate5 = createddate5;
		this.updatedby5 = updatedby5;
		this.updateddate5 = updateddate5;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_position_leave_idpositionleave_seq")
	@SequenceGenerator(name="generator_position_leave_idpositionleave_seq", sequenceName="position_leave_idpositionleave_seq", schema = "public", allocationSize = 1)
	@Column(name = "idpositionleave", unique = true, nullable = false)
	public long getIdpositionleave() {
		return this.idpositionleave;
	}

	public void setIdpositionleave(long idpositionleave) {
		this.idpositionleave = idpositionleave;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idposition")
	public Positions getPositions() {
		return this.positions;
	}

	public void setPositions(Positions positions) {
		this.positions = positions;
	}

	@Column(name = "jumlahcuti")
	public Short getJumlahcuti() {
		return this.jumlahcuti;
	}

	public void setJumlahcuti(Short jumlahcuti) {
		this.jumlahcuti = jumlahcuti;
	}

	@Column(name = "createdby5", updatable = false)
	public String getCreatedby5() {
		return this.createdby5;
	}

	public void setCreatedby5(String createdby5) {
		this.createdby5 = createdby5;
	}

	@Column(nullable = false, updatable = false, name = "createddate5", length = 13)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
	public Date getCreateddate5() {
		return this.createddate5;
	}

	public void setCreateddate5(Date createddate5) {
		this.createddate5 = createddate5;
	}

	@Column(name = "updatedby5")
	public String getUpdatedby5() {
		return this.updatedby5;
	}

	public void setUpdatedby5(String updatedby5) {
		this.updatedby5 = updatedby5;
	}

	@Column(name = "updateddate5", length = 13, nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
	public Date getUpdateddate5() {
		return this.updateddate5;
	}

	public void setUpdateddate5(Date updateddate5) {
		this.updateddate5 = updateddate5;
	}

}
