package com.userLeave.userLeaveReno.dto;
// Generated Apr 7, 2020 3:19:01 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;

public class UsersDTO{

	private long iduser;
	private PositionsDTO positions;
	private String namauser;
	private String createdby;
	private Date createddate;
	private String updatedby;
	private Date updateddate;

	public UsersDTO() {
	}

	public UsersDTO(long iduser, PositionsDTO positions) {
		this.iduser = iduser;
		this.positions = positions;
	}

	public UsersDTO(long iduser, PositionsDTO positions, String namauser, String createdby, Date createddate,
			String updatedby, Date updateddate) {
		this.iduser = iduser;
		this.positions = positions;
		this.namauser = namauser;
		this.createdby = createdby;
		this.createddate = createddate;
		this.updatedby = updatedby;
		this.updateddate = updateddate;
	}

	public long getIduser() {
		return this.iduser;
	}

	public void setIduser(long iduser) {
		this.iduser = iduser;
	}

	public PositionsDTO getPositions() {
		return this.positions;
	}

	public void setPositions(PositionsDTO positions) {
		this.positions = positions;
	}

	public String getNamauser() {
		return this.namauser;
	}

	public void setNamauser(String namauser) {
		this.namauser = namauser;
	}

	public String getCreatedby() {
		return this.createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getCreateddate() {
		return this.createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public String getUpdatedby() {
		return this.updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdateddate() {
		return this.updateddate;
	}

	public void setUpdateddate(Date updateddate) {
		this.updateddate = updateddate;
	}

}
