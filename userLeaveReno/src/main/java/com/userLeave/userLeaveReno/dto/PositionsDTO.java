package com.userLeave.userLeaveReno.dto;
// Generated Apr 7, 2020 3:19:01 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;

public class PositionsDTO{

	private long idposition;
	private String namaposisi;
	private Long approvebyposisi;
	private String createdby4;
	private Date createddate4;
	private String updatedby4;
	private Date updateddate4;

	public PositionsDTO() {
	}

	public PositionsDTO(long idposition) {
		this.idposition = idposition;
	}

	public PositionsDTO(long idposition, String namaposisi, Long approvebyposisi, String createdby4, Date createddate4,
			String updatedby4, Date updateddate4) {
		this.idposition = idposition;
		this.namaposisi = namaposisi;
		this.approvebyposisi = approvebyposisi;
		this.createdby4 = createdby4;
		this.createddate4 = createddate4;
		this.updatedby4 = updatedby4;
		this.updateddate4 = updateddate4;
	}

	public long getIdposition() {
		return this.idposition;
	}

	public void setIdposition(long idposition) {
		this.idposition = idposition;
	}

	public String getNamaposisi() {
		return this.namaposisi;
	}

	public void setNamaposisi(String namaposisi) {
		this.namaposisi = namaposisi;
	}

	public Long getApprovebyposisi() {
		return this.approvebyposisi;
	}

	public void setApprovebyposisi(Long approvebyposisi) {
		this.approvebyposisi = approvebyposisi;
	}

	public String getCreatedby4() {
		return this.createdby4;
	}

	public void setCreatedby4(String createdby4) {
		this.createdby4 = createdby4;
	}

	public Date getCreateddate4() {
		return this.createddate4;
	}

	public void setCreateddate4(Date createddate4) {
		this.createddate4 = createddate4;
	}

	public String getUpdatedby4() {
		return this.updatedby4;
	}

	public void setUpdatedby4(String updatedby4) {
		this.updatedby4 = updatedby4;
	}

	public Date getUpdateddate4() {
		return this.updateddate4;
	}

	public void setUpdateddate4(Date updateddate4) {
		this.updateddate4 = updateddate4;
	}

}
