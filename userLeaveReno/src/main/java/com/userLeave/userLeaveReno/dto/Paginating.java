package com.userLeave.userLeaveReno.dto;
// Generated Apr 7, 2020 3:19:01 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;


public class Paginating{
	
	private long showId;
	private long userId;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private String status;
	private String resolverReason;
	private String resolvedBy;
	private Date resolvedDate;

	public Paginating() {
	}
	
	public Paginating(long showId, long userId, Date leaveDateFrom,
			Date leaveDateTo, String description, String status, String resolverReason, String resolvedBy,
			Date resolvedDate) {
		super();
		this.showId = showId;
		this.userId = userId;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.description = description;
		this.status = status;
		this.resolverReason = resolverReason;
		this.resolvedBy = resolvedBy;
		this.resolvedDate = resolvedDate;
	}

	public long getShowId() {
		return showId;
	}

	public void setShowId(long showId) {
		this.showId = showId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResolverReason() {
		return resolverReason;
	}

	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	@Override
	public String toString() {
		return "Paginating [userId=" + userId + ", leaveDateFrom=" + leaveDateFrom + ", leaveDateTo=" + leaveDateTo
				+ ", description=" + description + ", status=" + status + ", resolverReason=" + resolverReason
				+ ", resolvedBy=" + resolvedBy + ", resolvedDate=" + resolvedDate + "]";
	}

	
}
