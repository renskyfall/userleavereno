package com.userLeave.userLeaveReno.dto;
// Generated Apr 7, 2020 3:19:01 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;

public class BucketApprovalDTO{

	private long idbucketapproval;
	private UsersDTO users;
	private String bucketstatus;
	private String resolverreason;
	private Date resolvedate;
	private String createdby2;
	private Date createddate2;
	private String updatedby2;
	private Date updateddate2;

	public BucketApprovalDTO() {
	}

	public BucketApprovalDTO(long idbucketapproval) {
		this.idbucketapproval = idbucketapproval;
	}

	public BucketApprovalDTO(long idbucketapproval, UsersDTO users, String bucketstatus, String resolverreason,
			Date resolvedate, String createdby2, String updatedby2) {
		this.idbucketapproval = idbucketapproval;
		this.users = users;
		this.bucketstatus = bucketstatus;
		this.resolverreason = resolverreason;
		this.resolvedate = resolvedate;
		this.createdby2 = createdby2;
		this.updatedby2 = updatedby2;
	}

	
	public long getIdbucketapproval() {
		return this.idbucketapproval;
	}

	public void setIdbucketapproval(long idbucketapproval) {
		this.idbucketapproval = idbucketapproval;
	}

	public UsersDTO getUsers() {
		return this.users;
	}

	public void setUsers(UsersDTO users) {
		this.users = users;
	}

	public String getBucketstatus() {
		return this.bucketstatus;
	}

	public void setBucketstatus(String bucketstatus) {
		this.bucketstatus = bucketstatus;
	}

	public String getResolverreason() {
		return this.resolverreason;
	}

	public void setResolverreason(String resolverreason) {
		this.resolverreason = resolverreason;
	}

	public Date getResolvedate() {
		return this.resolvedate;
	}

	public void setResolvedate(Date resolvedate) {
		this.resolvedate = resolvedate;
	}

	public String getCreatedby2() {
		return this.createdby2;
	}

	public void setCreatedby2(String createdby2) {
		this.createdby2 = createdby2;
	}

	public Date getCreateddate2() {
		return this.createddate2;
	}

	public void setCreateddate2(Date createddate2) {
		this.createddate2 = createddate2;
	}

	public String getUpdatedby2() {
		return this.updatedby2;
	}

	public void setUpdatedby2(String updatedby2) {
		this.updatedby2 = updatedby2;
	}
	
	public Date getUpdateddate2() {
		return this.updateddate2;
	}

	public void setUpdateddate2(Date updateddate2) {
		this.updateddate2 = updateddate2;
	}


}
