package com.userLeave.userLeaveReno.dto;
// Generated Apr 7, 2020 3:19:01 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;

public class PositionLeaveDTO{

	private long idpositionleave;
	private PositionsDTO positions;
	private Short jumlahcuti;
	private String createdby5;
	private Date createddate5;
	private String updatedby5;
	private Date updateddate5;

	public PositionLeaveDTO() {
	}

	public PositionLeaveDTO(long idpositionleave) {
		this.idpositionleave = idpositionleave;
	}

	public PositionLeaveDTO(long idpositionleave, PositionsDTO positions, Short jumlahcuti, String createdby5,
			Date createddate5, String updatedby5, Date updateddate5) {
		this.idpositionleave = idpositionleave;
		this.positions = positions;
		this.jumlahcuti = jumlahcuti;
		this.createdby5 = createdby5;
		this.createddate5 = createddate5;
		this.updatedby5 = updatedby5;
		this.updateddate5 = updateddate5;
	}

	public long getIdpositionleave() {
		return this.idpositionleave;
	}

	public void setIdpositionleave(long idpositionleave) {
		this.idpositionleave = idpositionleave;
	}

	public PositionsDTO getPositions() {
		return this.positions;
	}

	public void setPositions(PositionsDTO positions) {
		this.positions = positions;
	}

	public Short getJumlahcuti() {
		return this.jumlahcuti;
	}

	public void setJumlahcuti(Short jumlahcuti) {
		this.jumlahcuti = jumlahcuti;
	}

	public String getCreatedby5() {
		return this.createdby5;
	}

	public void setCreatedby5(String createdby5) {
		this.createdby5 = createdby5;
	}

	public Date getCreateddate5() {
		return this.createddate5;
	}

	public void setCreateddate5(Date createddate5) {
		this.createddate5 = createddate5;
	}

	public String getUpdatedby5() {
		return this.updatedby5;
	}

	public void setUpdatedby5(String updatedby5) {
		this.updatedby5 = updatedby5;
	}

	public Date getUpdateddate5() {
		return this.updateddate5;
	}

	public void setUpdateddate5(Date updateddate5) {
		this.updateddate5 = updateddate5;
	}

}
