package com.userLeave.userLeaveReno.dto;
// Generated Apr 7, 2020 3:19:01 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;


public class UserLeaveRequestDTO{

	private long iduserleaverequest;
	private BucketApprovalDTO bucketApproval;
	private UsersDTO users;
	private Date leavedatefrom;
	private Date leavedateto;
	private String description;
	private Short cutitersedia;
	private String createdby3;
	private Date createddate3;
	private String updatedby3;
	private Date updateddate3;

	public UserLeaveRequestDTO() {
	}

	public UserLeaveRequestDTO(long iduserleaverequest) {
		this.iduserleaverequest = iduserleaverequest;
	}

	public UserLeaveRequestDTO(long iduserleaverequest, BucketApprovalDTO bucketApproval, UsersDTO users, Date leavedatefrom,
			Date leavedateto, String description, Short cutitersedia, String createdby3, Date createddate3,
			String updatedby3, Date updateddate3) {
		this.iduserleaverequest = iduserleaverequest;
		this.bucketApproval = bucketApproval;
		this.users = users;
		this.leavedatefrom = leavedatefrom;
		this.leavedateto = leavedateto;
		this.description = description;
		this.cutitersedia = cutitersedia;
		this.createdby3 = createdby3;
		this.createddate3 = createddate3;
		this.updatedby3 = updatedby3;
		this.updateddate3 = updateddate3;
	}
	
	public long getIduserleaverequest() {
		return this.iduserleaverequest;
	}

	public void setIduserleaverequest(long iduserleaverequest) {
		this.iduserleaverequest = iduserleaverequest;
	}

	public BucketApprovalDTO getBucketApproval() {
		return this.bucketApproval;
	}

	public void setBucketApproval(BucketApprovalDTO bucketApproval) {
		this.bucketApproval = bucketApproval;
	}

	public UsersDTO getUsers() {
		return this.users;
	}

	public void setUsers(UsersDTO users) {
		this.users = users;
	}

	public Date getLeavedatefrom() {
		return this.leavedatefrom;
	}

	public void setLeavedatefrom(Date leavedatefrom) {
		this.leavedatefrom = leavedatefrom;
	}

	public Date getLeavedateto() {
		return this.leavedateto;
	}

	public void setLeavedateto(Date leavedateto) {
		this.leavedateto = leavedateto;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Short getCutitersedia() {
		return this.cutitersedia;
	}

	public void setCutitersedia(Short cutitersedia) {
		this.cutitersedia = cutitersedia;
	}

	public String getCreatedby3() {
		return this.createdby3;
	}

	public void setCreatedby3(String createdby3) {
		this.createdby3 = createdby3;
	}

	public Date getCreateddate3() {
		return this.createddate3;
	}

	public void setCreateddate3(Date createddate3) {
		this.createddate3 = createddate3;
	}

	public String getUpdatedby3() {
		return this.updatedby3;
	}

	public void setUpdatedby3(String updatedby3) {
		this.updatedby3 = updatedby3;
	}

	public Date getUpdateddate3() {
		return this.updateddate3;
	}

	public void setUpdateddate3(Date updateddate3) {
		this.updateddate3 = updateddate3;
	}

}
