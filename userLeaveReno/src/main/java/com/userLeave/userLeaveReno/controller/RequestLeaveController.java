package com.userLeave.userLeaveReno.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.userLeave.userLeaveReno.dto.BucketApprovalDTO;
import com.userLeave.userLeaveReno.dto.PositionsDTO;
import com.userLeave.userLeaveReno.dto.UserLeaveRequestDTO;
import com.userLeave.userLeaveReno.entity.BucketApproval;
import com.userLeave.userLeaveReno.entity.PositionLeave;
import com.userLeave.userLeaveReno.entity.Positions;
import com.userLeave.userLeaveReno.entity.UserLeaveRequest;
import com.userLeave.userLeaveReno.entity.Users;
import com.userLeave.userLeaveReno.repository.BucketApprovalRepository;
import com.userLeave.userLeaveReno.repository.PositionLeaveRepository;
import com.userLeave.userLeaveReno.repository.PositionsRepository;
import com.userLeave.userLeaveReno.repository.UserLeaveRequestRepository;
import com.userLeave.userLeaveReno.repository.UsersRepository;

@RestController
@RequestMapping("/api")
public class RequestLeaveController {
	 
	 ModelMapper modelMapper = new ModelMapper();
	 
	 public UserLeaveRequestDTO convertToDTO(UserLeaveRequest user) {
		 UserLeaveRequestDTO userDto = modelMapper.map(user, UserLeaveRequestDTO.class);
		 return userDto;
	 }
	    
     private UserLeaveRequest convertToEntity(UserLeaveRequestDTO userDto) {
    	 UserLeaveRequest user = modelMapper.map(userDto, UserLeaveRequest.class);
    	 return user;
     }
     
	 @Autowired
	 UsersRepository usersRepository;
	 @Autowired
	 PositionsRepository positionsRepository;
	 @Autowired
	 PositionLeaveRepository positionLeaveRepository;
	 @Autowired
	 BucketApprovalRepository bucketApprovalRepository;
	 @Autowired
	 UserLeaveRequestRepository userLeaveRequestRepository;
	 
	 //Get All User
	 @GetMapping("/listRequestLeave/{userId}/{choosenPage}/{totalDataPerPage}")
	 public HashMap<String, Object> getAllUser(@PathVariable(value = "userId") Long id, @PathVariable(value = "totalDataPerPage") int totalData, @PathVariable(value = "choosenPage") int choosenPage) {
		Pageable paging = PageRequest.of(totalData, choosenPage);
		
		Page<UserLeaveRequest> pagedResult = userLeaveRequestRepository.findAll(paging);
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<UserLeaveRequestDTO> listUsers = new ArrayList<UserLeaveRequestDTO>();
		for(UserLeaveRequest tempPositions : pagedResult.getContent()) {
			UserLeaveRequestDTO positionDTO = convertToDTO(tempPositions);
			listUsers.add(positionDTO);
		}
		
		String message;
		if(listUsers.isEmpty()) {
			message = "Read All Failed!";
	   	} else {
	   		message = "Read All Success!";
	   	}
		
	   	showHashMap.put("Message", message);
	   	showHashMap.put("Total", listUsers.size());
	   	showHashMap.put("Data", listUsers);
		
		return showHashMap;
	 }
	 
	 @GetMapping("/bucketapproval/all")
	 public HashMap<String, Object> getAllBucketApproval() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<BucketApprovalDTO> listUsers = new ArrayList<BucketApprovalDTO>();
		for(BucketApproval tempPositions : bucketApprovalRepository.findAll()) {
			BucketApprovalDTO positionDTO = modelMapper.map(tempPositions, BucketApprovalDTO.class);;
			listUsers.add(positionDTO);
		}
		
		String message;
		if(listUsers.isEmpty()) {
			message = "Read All Failed!";
	   	} else {
	   		message = "Read All Success!";
	   	}
		
	   	showHashMap.put("Message", message);
	   	showHashMap.put("Total", listUsers.size());
	   	showHashMap.put("Data", listUsers);
		
		return showHashMap;
	 }
	 
	 @PostMapping("/requestLeave/{id}/create")
	 public HashMap<String, Object> addLeaveRequest(@PathVariable(value = "id") Long id, @RequestBody UserLeaveRequest inputLeave){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Users user = usersRepository.findById(id).orElse(null);
		 
		UserLeaveRequest userLeaveRequest = inputLeave;
		userLeaveRequest.setCutitersedia((short)getSisaCuti(userLeaveRequest.getUsers().getIduser()));
		userLeaveRequest.setCreatedby3(user.getNamauser());
		userLeaveRequest.setUpdatedby3(user.getNamauser());
		LocalDate today = LocalDate.now();
		LocalDate dateFrom = userLeaveRequest.getLeavedatefrom().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate dateTo = userLeaveRequest.getLeavedateto().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		boolean isWaiting = false;
		for(UserLeaveRequest ulr: userLeaveRequestRepository.findAll()) {
			if(inputLeave.getUsers().getIduser() == ulr.getUsers().getIduser()) {
				if(ulr.getBucketApproval().getBucketstatus().equalsIgnoreCase("waiting")) {
					isWaiting = true;
				}
			}
		}
		if(isWaiting) {
			showHashMap.put("Messages", "Anda masih memiliki cuti yang belum di resolve, harap hubungi pihak terkait");
		}else {
			if(today.compareTo(dateFrom) == 1) {
				showHashMap.put("Messages", "Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti anda.");
			}else {
				if(dateFrom.compareTo(dateTo) == 1) {
					showHashMap.put("Messages", "Tanggal yang Anda ajukan tidak valid.");
				}else {
					if(userLeaveRequest.getCutitersedia() == null || userLeaveRequest.getCutitersedia() == 0) {
						showHashMap.put("Messages", "Mohon maaf, jatah cuti Anda telah habis.");
					}else {
						int jumlahHari = (int)ChronoUnit.DAYS.between(dateFrom, dateTo);
						
						if(jumlahHari > userLeaveRequest.getCutitersedia()) {
							showHashMap.put("Messages", "Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari tanggal "+dateFrom+" sampai "+dateTo+" ("+jumlahHari+" hari). Jatah cuti Anda yang tersisa adalah "+userLeaveRequest.getCutitersedia()+" hari.");
						}else {
							userLeaveRequest.setBucketApproval(createBucketApproval(id));
							showHashMap.put("Messages", "Permohonan Anda sedang diproses.");
							userLeaveRequestRepository.save(userLeaveRequest);
							showHashMap.put("Data", convertToDTO(userLeaveRequest));
						}
					}
				}
			}
		}
		
		
		
		 
		return showHashMap;
	 }
	 
	 @PostMapping("/resolveRequestLeave/{id}")
	 public HashMap<String, Object> addBucketApproval(@PathVariable(value = "id") Long id, @RequestBody BucketApproval inputData){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Users user = usersRepository.findById(id).orElse(null);
		BucketApproval approval = bucketApprovalRepository.findById(inputData.getIdbucketapproval()).orElse(null);
		if(approval == null) {
			showHashMap.put("Messages", "Permohonan dengan ID "+inputData.getIdbucketapproval()+" tidak ditemukan.");
		}else {
			approval = inputData;
			for(UserLeaveRequest ulr: userLeaveRequestRepository.findAll()) {
				if(ulr.getBucketApproval().getIdbucketapproval() == approval.getIdbucketapproval()) {		
					if(ulr.getLeavedatefrom().compareTo(inputData.getResolvedate()) == 1) {
						showHashMap.put("Messages", "Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti.");
					}else {
						
						if(user.getPositions().getIdposition() == ulr.getUsers().getPositions().getApprovebyposisi()) {
							if(ulr.getUsers() != user || user.getIduser() == 3) {
								approval.setUpdatedby2(user.getNamauser());
								approval.setUsers(user);
								bucketApprovalRepository.save(approval);
								showHashMap.put("Messages", "Permohonan dengan ID "+approval.getIdbucketapproval()+" telah berhasil diputuskan.");
							}else {
								showHashMap.put("Messages", "Anda tidak berwenang me-resolve pengajuan cuti.");
							}
						}else {
							showHashMap.put("Messages", "Anda tidak berwenang me-resolve pengajuan cuti.");
						}
						
					}
				}
			}
			
			
		}
		return showHashMap;
	 }
	 
	 @DeleteMapping("/deleteRequestLeave/{id}")
	 public HashMap<String, Object> deleteLeaveRequest(@PathVariable(value = "id") Long id){
			HashMap<String, Object> showHashMap = new HashMap<String, Object>();
			UserLeaveRequest positions = userLeaveRequestRepository.findById(id).orElse(null);
			UserLeaveRequestDTO positionsDTO = convertToDTO(positions);
			userLeaveRequestRepository.delete(positions);
			bucketApprovalRepository.delete(positions.getBucketApproval());

		    showHashMap.put("Messages", "Delete Data Success!");
		    showHashMap.put("Delete data :", positionsDTO);
		   	return showHashMap;
	 }
	 public BucketApproval createBucketApproval(Long id) {
		 Users user = usersRepository.findById(id).orElse(null);
		 BucketApproval bucketApproval = new BucketApproval();
		 bucketApproval.setBucketstatus("waiting");
		 bucketApproval.setCreatedby2(user.getNamauser());
		 bucketApproval.setUpdatedby2(user.getNamauser());
		 return bucketApprovalRepository.save(bucketApproval);
	 }
	 
	 @GetMapping("/sisacuti/{id}")
	 public HashMap<String, Object> getInformasiSisaCuti(@PathVariable(value = "id") Long id) {
		 HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		 Users user = usersRepository.findById(id).orElse(null);
		 int jumlahCuti = getSisaCuti(id);
		 showHashMap.put("Message", "Jumlah cuti yang "+user.getNamauser()+" miliki saat ini adalah "+jumlahCuti+" hari");
		 return showHashMap;
	 }
	
	 public int getSisaCuti(Long id) {
		 Users user = usersRepository.findById(id).orElse(null);
		 PositionLeave positionLeave = getPositionLeaveByPosition(user.getPositions());
		 int cutiKaryawan = positionLeave.getJumlahcuti();
		 for(UserLeaveRequest ulr: userLeaveRequestRepository.findAll()) {
			 if(ulr.getUsers().getIduser() == user.getIduser()) {
				 LocalDate dateFrom = ulr.getLeavedatefrom().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				 LocalDate dateTo = ulr.getLeavedateto().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				 int jumlahHari = (int)ChronoUnit.DAYS.between(dateFrom, dateTo);
				 if(ulr.getBucketApproval().getBucketstatus().equalsIgnoreCase("approved")) {
					 cutiKaryawan -= jumlahHari;
				 }
			 }
		 }
		 return cutiKaryawan;
	 }
	 
	 public PositionLeave getPositionLeaveByPosition(Positions position) {
		 PositionLeave positionLeave = new PositionLeave();
		 for(PositionLeave pl : positionLeaveRepository.findAll()) {
			 if(pl.getPositions() == position) {
				 positionLeave = pl;
			 }
		 }
		 return positionLeave;
	 }
	 
		 

     
   
}
	 
