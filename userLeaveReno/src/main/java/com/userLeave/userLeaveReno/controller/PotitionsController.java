package com.userLeave.userLeaveReno.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.userLeave.userLeaveReno.dto.PositionsDTO;
import com.userLeave.userLeaveReno.entity.Positions;
import com.userLeave.userLeaveReno.repository.PositionsRepository;

@RestController
@RequestMapping("/api")
public class PotitionsController {
	 
	 ModelMapper modelMapper = new ModelMapper();
	 
	 @Autowired
	 PositionsRepository positionsRepository;
	 
	 public PositionsDTO convertToDTO(Positions position) {
		 PositionsDTO positionDto = modelMapper.map(position, PositionsDTO.class);
        return positionDto;
	 }
	    
   private Positions convertToEntity(PositionsDTO positionDto) {
	   Positions position = modelMapper.map(positionDto, Positions.class);
       return position;
   }
		 
	 //Get All User
	 @GetMapping("/positions/all")
	 public HashMap<String, Object> getAllUser() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<PositionsDTO> listUsers = new ArrayList<PositionsDTO>();
		for(Positions tempPositions : positionsRepository.findAll()) {
			PositionsDTO positionDTO = convertToDTO(tempPositions);
			listUsers.add(positionDTO);
		}
		
		String message;
		if(listUsers.isEmpty()) {
			message = "Read All Failed!";
	   	} else {
	   		message = "Read All Success!";
	   	}
		
	   	showHashMap.put("Message", message);
	   	showHashMap.put("Total", listUsers.size());
	   	showHashMap.put("Data", listUsers);
		
		return showHashMap;
	 }
	 
	 // Read User By ID
	 @GetMapping("/positions/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Positions posisi = positionsRepository.findById(id).orElse(null);
		PositionsDTO posisiDTO = convertToDTO(posisi);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", posisiDTO);
		return showHashMap;
	}
	 
	// Create a new User
	@PostMapping("/positions/add/{name}")
	public HashMap<String, Object> createUser(@Valid @RequestBody PositionsDTO positionDTO, @PathVariable(value = "name") String name) {
	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
	PositionsDTO resultPositionDTO = new PositionsDTO();
   	if(positionDTO.getNamaposisi() == null || positionDTO.getNamaposisi().equals("")) {
   		showHashMap.put("Message : ", "Create Failed, please fill the namaPosisi and try again!");
   	}else {
   		showHashMap.put("Message", "Create Succes");
   		positionDTO.setCreatedby4(name);
   		positionDTO.setUpdatedby4(name);
   		resultPositionDTO = convertToDTO(positionsRepository.save(convertToEntity(positionDTO)));
   	   	showHashMap.put("Data", resultPositionDTO);
   	}
	   			
   	return showHashMap;
   }
	
	// Update a User
   @PutMapping("/positions/{id}/{name}")
   public HashMap<String, Object> update(@PathVariable(value = "id") Long id, @PathVariable(value = "name") String updatedName, @Valid @RequestBody PositionsDTO positionsDto) {
	HashMap<String, Object> process = new HashMap<String, Object>();
	Positions tempPositions = positionsRepository.findById(id).orElse(null);      
	
	positionsDto.setIdposition(tempPositions.getIdposition());
	positionsDto.setUpdatedby4(updatedName);
    if (positionsDto.getNamaposisi() == null) {
    	positionsDto.setNamaposisi(tempPositions.getNamaposisi());
    }
    if (positionsDto.getApprovebyposisi() == null) {
    	positionsDto.setApprovebyposisi(convertToDTO(tempPositions).getApprovebyposisi());
    }
    
    tempPositions = convertToEntity(positionsDto);
    if(tempPositions == null) {
    	process.put("Message", "Failed Update Data");
    }else {
    	process.put("Message", "Success Updated Data");
    }
    positionsDto = convertToDTO(tempPositions);
    positionsRepository.save(tempPositions);
    process.put("Data", positionsDto);
    return process;
}
   
   // Delete a User
   @DeleteMapping("/positions/{id}")
   public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
   	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
   	Positions positions = positionsRepository.findById(id).orElse(null);
   	PositionsDTO positionsDTO = convertToDTO(positions);
   	positionsRepository.delete(positions);

    showHashMap.put("Messages", "Delete Data Success!");
    showHashMap.put("Delete data :", positionsDTO);
   	return showHashMap;
   }
     
   
}
	 
