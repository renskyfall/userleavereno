package com.userLeave.userLeaveReno.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.userLeave.userLeaveReno.dto.UsersDTO;
import com.userLeave.userLeaveReno.entity.Users;
import com.userLeave.userLeaveReno.repository.UsersRepository;

@RestController
@RequestMapping("/api")
public class UsersController {
	 
	 ModelMapper modelMapper = new ModelMapper();
	 
	 @Autowired
	 UsersRepository usersRepository;
	 
	 public UsersDTO convertToDTO(Users user) {
		 UsersDTO userDto = modelMapper.map(user, UsersDTO.class);
        return userDto;
	 }
	    
     private Users convertToEntity(UsersDTO userDto) {
	   Users user = modelMapper.map(userDto, Users.class);
       return user;
     }
		 
	 //Get All User
	 @GetMapping("/users/all")
	 public HashMap<String, Object> getAllUser() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<UsersDTO> listUsers = new ArrayList<UsersDTO>();
		for(Users tempUsers : usersRepository.findAll()) {
			UsersDTO userDTO = convertToDTO(tempUsers);
			listUsers.add(userDTO);
		}
		
		String message;
		if(listUsers.isEmpty()) {
			message = "Read All Failed!";
	   	} else {
	   		message = "Read All Success!";
	   	}
		
	   	showHashMap.put("Message", message);
	   	showHashMap.put("Total", listUsers.size());
	   	showHashMap.put("Data", listUsers);
		
		return showHashMap;
	 }
	 
	 // Read User By ID
	 @GetMapping("/users/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		Users user = usersRepository.findById(id).orElse(null);
		UsersDTO userDTO = convertToDTO(user);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", userDTO);
		return showHashMap;
	}
	 
	// Create a new User
	@PostMapping("/users/add/{name}")
	public HashMap<String, Object> createUser(@Valid @RequestBody UsersDTO userDTO, @PathVariable(value = "name") String name) {
	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
	UsersDTO resultUserDTO = new UsersDTO();
   	if(userDTO.getNamauser() == null) {
   		showHashMap.put("Message : ", "Create Failed, please fill the namaUser and try again!");
   	}else {
   		showHashMap.put("Message", "Create Succes");
   		userDTO.setCreatedby(name);
   		userDTO.setUpdatedby(name);
   		resultUserDTO = convertToDTO(usersRepository.save(convertToEntity(userDTO)));
   	   	showHashMap.put("Data", resultUserDTO);
   	}
	   			
   	return showHashMap;
   }
	
	// Update a User
   @PutMapping("/users/{id}/{name}")
   public HashMap<String, Object> update(@PathVariable(value = "id") Long id, @PathVariable(value = "name") String updatedName, @Valid @RequestBody UsersDTO usersDto) {
	HashMap<String, Object> process = new HashMap<String, Object>();
	Users tempUsers = usersRepository.findById(id).orElse(null);      
	
	usersDto.setIduser(tempUsers.getIduser());
	usersDto.setUpdatedby(updatedName);
    if (usersDto.getNamauser() == null) {
    	usersDto.setNamauser(tempUsers.getNamauser());
    }
    if (usersDto.getPositions() == null) {
    	usersDto.setPositions(convertToDTO(tempUsers).getPositions());
    }
    
    tempUsers = convertToEntity(usersDto);
    if(tempUsers == null) {
    	process.put("Message", "Failed Update Data");
    }else {
    	process.put("Message", "Success Updated Data");
    }
    usersDto = convertToDTO(tempUsers);
    usersRepository.save(tempUsers);
    process.put("Data", usersDto);
    return process;
}
   
   // Delete a User
   @DeleteMapping("/users/{id}")
   public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
   	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
   	Users users = usersRepository.findById(id).orElse(null);
   	UsersDTO usersDTO = convertToDTO(users);
   	usersRepository.delete(users);

    showHashMap.put("Messages", "Delete Data Success!");
    showHashMap.put("Delete data :", usersDTO);
   	return showHashMap;
   }
     
   
}
	 
