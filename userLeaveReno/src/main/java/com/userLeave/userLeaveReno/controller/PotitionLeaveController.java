package com.userLeave.userLeaveReno.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.userLeave.userLeaveReno.dto.PositionLeaveDTO;
import com.userLeave.userLeaveReno.entity.PositionLeave;
import com.userLeave.userLeaveReno.repository.PositionLeaveRepository;

@RestController
@RequestMapping("/api")
public class PotitionLeaveController {
	 
	 ModelMapper modelMapper = new ModelMapper();
	 
	 @Autowired
	 PositionLeaveRepository positionLeaveRepository;
	 
	 public PositionLeaveDTO convertToDTO(PositionLeave positionLeave) {
		 PositionLeaveDTO positionLeaveDto = modelMapper.map(positionLeave, PositionLeaveDTO.class);
        return positionLeaveDto;
	 }
	    
   private PositionLeave convertToEntity(PositionLeaveDTO positionLeaveDto) {
	   PositionLeave positionLeave = modelMapper.map(positionLeaveDto, PositionLeave.class);
       return positionLeave;
   }
		 
	 //Get All User
	 @GetMapping("/positionLeave/all")
	 public HashMap<String, Object> getAllUser() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<PositionLeaveDTO> listUsers = new ArrayList<PositionLeaveDTO>();
		for(PositionLeave tempPositions : positionLeaveRepository.findAll()) {
			PositionLeaveDTO positionDTO = convertToDTO(tempPositions);
			listUsers.add(positionDTO);
		}
		
		String message;
		if(listUsers.isEmpty()) {
			message = "Read All Failed!";
	   	} else {
	   		message = "Read All Success!";
	   	}
		
	   	showHashMap.put("Message", message);
	   	showHashMap.put("Total", listUsers.size());
	   	showHashMap.put("Data", listUsers);
		
		return showHashMap;
	 }
	 
	 // Read User By ID
	 @GetMapping("/positionLeave/{id}")
	 public HashMap<String, Object> getById(@PathVariable(value = "id") Long id){
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		PositionLeave posisi = positionLeaveRepository.findById(id).orElse(null);
		PositionLeaveDTO posisiDTO = convertToDTO(posisi);
		showHashMap.put("Messages", "Read Data Success");
		showHashMap.put("Data", posisiDTO);
		return showHashMap;
	}
	 
	// Create a new User
	@PostMapping("/positionLeave/add/{name}")
	public HashMap<String, Object> createUser(@Valid @RequestBody PositionLeaveDTO positionDTO, @PathVariable(value = "name") String name) {
	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
	PositionLeaveDTO resultPositionDTO = new PositionLeaveDTO();
   	if(positionDTO.getPositions() == null) {
   		showHashMap.put("Message : ", "Create Failed, please fill the Posisi and try again!");
   	}else {
   		showHashMap.put("Message", "Create Succes");
   		positionDTO.setCreatedby5(name);
   		positionDTO.setUpdatedby5(name);
   		resultPositionDTO = convertToDTO(positionLeaveRepository.save(convertToEntity(positionDTO)));
   	   	showHashMap.put("Data", resultPositionDTO);
   	}
	   			
   	return showHashMap;
   }
	
	// Update a User
   @PutMapping("/positionLeave/{id}/{name}")
   public HashMap<String, Object> update(@PathVariable(value = "id") Long id, @PathVariable(value = "name") String updatedName, @Valid @RequestBody PositionLeaveDTO positionsDto) {
	HashMap<String, Object> process = new HashMap<String, Object>();
	PositionLeave tempPositions = positionLeaveRepository.findById(id).orElse(null);      
	
	positionsDto.setIdpositionleave(tempPositions.getIdpositionleave());
	positionsDto.setUpdatedby5(updatedName);
    if (positionsDto.getPositions() == null) {
    	positionsDto.setPositions(convertToDTO(tempPositions).getPositions());
    }
    if (positionsDto.getJumlahcuti() == null) {
    	positionsDto.setJumlahcuti(tempPositions.getJumlahcuti());
    }
    
    tempPositions = convertToEntity(positionsDto);
    if(tempPositions == null) {
    	process.put("Message", "Failed Update Data");
    }else {
    	process.put("Message", "Success Updated Data");
    }
    positionsDto = convertToDTO(tempPositions);
    positionLeaveRepository.save(tempPositions);
    process.put("Data", positionsDto);
    return process;
}
   
   // Delete a User
   @DeleteMapping("/positionLeave/{id}")
   public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
   	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
   	PositionLeave positions = positionLeaveRepository.findById(id).orElse(null);
   	PositionLeaveDTO positionsDTO = convertToDTO(positions);
   	positionLeaveRepository.delete(positions);

    showHashMap.put("Messages", "Delete Data Success!");
    showHashMap.put("Delete data :", positionsDTO);
   	return showHashMap;
   }
     
   
}
	 
