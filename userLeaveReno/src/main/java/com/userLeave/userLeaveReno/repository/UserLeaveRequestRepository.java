package com.userLeave.userLeaveReno.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.userLeave.userLeaveReno.entity.UserLeaveRequest;


@Repository
public interface UserLeaveRequestRepository extends JpaRepository<UserLeaveRequest, Long>{
	

}
