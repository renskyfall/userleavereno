package com.userLeave.userLeaveReno.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.userLeave.userLeaveReno.entity.PositionLeave;


@Repository
public interface PositionLeaveRepository extends JpaRepository<PositionLeave, Long>{
	

}
