package com.userLeave.userLeaveReno.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.userLeave.userLeaveReno.entity.BucketApproval;


@Repository
public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Long>{
	

}
