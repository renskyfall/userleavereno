package com.userLeave.userLeaveReno.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.userLeave.userLeaveReno.entity.Users;


@Repository
public interface UsersRepository extends JpaRepository<Users, Long>{
	

}
