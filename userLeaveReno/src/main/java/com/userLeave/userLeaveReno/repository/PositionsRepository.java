package com.userLeave.userLeaveReno.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.userLeave.userLeaveReno.entity.Positions;


@Repository
public interface PositionsRepository extends JpaRepository<Positions, Long>{
	

}
